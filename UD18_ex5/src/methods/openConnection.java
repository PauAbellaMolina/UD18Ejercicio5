package methods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class openConnection {
	
	public static Connection conexion;

	public static void openConnection() {
	
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.106:3306?useTimezone=true&serverTimezone=UTC","user","pass" );
			System.out.println("Server Connected");
	}catch(SQLException | ClassNotFoundException ex) {
		
		System.out.println("No se ha podido connectar con mi base de datos");
		System.out.println(ex);
	}
		
	}
}
