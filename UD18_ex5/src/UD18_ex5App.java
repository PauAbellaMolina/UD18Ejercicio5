import java.sql.Connection;

import dto.DatabaseDto;
import dto.createTableDto;
import methods.closeConnection;
import methods.createDB;
import methods.createTable;
import methods.insertData;
import methods.openConnection;

public class UD18_ex5App {
	
	public static Connection conexion;

	public static void main(String[] args) {
	
        DatabaseDto newDatabase = new DatabaseDto("ejercicio5");
        
        createTableDto DESPACHOS = new createTableDto(newDatabase, "DESPACHOS");
        createTableDto DIRECTORES = new createTableDto(newDatabase, "DIRECTORES");
        
        String CreacionTabla1 = "CREATE TABLE `ejercicio5`.`DESPACHOS` (`Numero` INT NOT NULL,`Capacidad` INT NULL,PRIMARY KEY (`Numero`));";
        String CreacionTabla2 = "CREATE TABLE `ejercicio5`.`DIRECTORES` (`DNI` VARCHAR(8) NOT NULL,`NomApels` VARCHAR(255) NULL,`DNIJefe` VARCHAR(8) NULL,`Despacho` INT NULL,PRIMARY KEY (`DNI`));";
        
        openConnection.openConnection();
        createDB.createDB(newDatabase);
        createTable.createTable(newDatabase, DESPACHOS, CreacionTabla1);
        createTable.createTable(newDatabase, DIRECTORES, CreacionTabla2);
        insertData.insertData(newDatabase, "DESPACHOS", "(Numero, Capacidad) VALUE(3, 36);");
        closeConnection.closeConnection();
	}

}
